import 'dart:io';

import 'package:camerawesome/models/orientations.dart';
import 'package:camerawesome_example/widgets/bottom_bar.dart';
import 'package:camerawesome_example/widgets/camera_buttons.dart';
import 'package:camerawesome_example/widgets/preview_card.dart';
import 'package:camerawesome_example/widgets/top_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:camerawesome/camerawesome_plugin.dart';

void main() {
  runApp(MaterialApp(
    home: CameraWidget(),
    debugShowCheckedModeBanner: false,
  ));
}

class CameraWidget extends StatefulWidget {
  @override
  _CameraWidgetState createState() => _CameraWidgetState();
}

class _CameraWidgetState extends State<CameraWidget>
    with TickerProviderStateMixin {
  String _lastPhotoPath;

  ValueNotifier<CameraFlashes> _switchFlash = ValueNotifier(CameraFlashes.NONE);

  ValueNotifier<Size> _photoSize = ValueNotifier(null);

  ValueNotifier<Sensors> _sensor = ValueNotifier(Sensors.BACK);

  ValueNotifier<CaptureModes> _captureMode = ValueNotifier(CaptureModes.PHOTO);

  ValueNotifier<CameraOrientations> _orientation =
      ValueNotifier(CameraOrientations.PORTRAIT_UP);

  PictureController _pictureController;

  AnimationController _iconsAnimationController, _previewAnimationController;

  Animation<Offset> _previewAnimation;

  bool _isTakingPicture;

  _onChangeSensorTap() {
    _sensor.value =
        _sensor.value == Sensors.FRONT ? Sensors.BACK : Sensors.FRONT;
  }

  _updateTakingPictureStatus(bool newStatus) {
    this._isTakingPicture = newStatus;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _isTakingPicture = false;
    _pictureController = PictureController();
    _iconsAnimationController = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 300),
    );

    _previewAnimationController = AnimationController(
      duration: const Duration(milliseconds: 1300),
      vsync: this,
    );

    _previewAnimation = Tween<Offset>(
      begin: const Offset(-2.0, 0.0),
      end: Offset.zero,
    ).animate(
      CurvedAnimation(
        parent: _previewAnimationController,
        curve: Curves.elasticOut,
        reverseCurve: Curves.elasticIn,
      ),
    );
  }

  @override
  void dispose() {
    _iconsAnimationController.dispose();
    _previewAnimationController.dispose();
    _photoSize.dispose();
    _captureMode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          _buildCameraFull(),
          _buildInterface(),
          PreviewCardWidget(
            lastPhotoPath: _lastPhotoPath,
            orientation: _orientation,
            previewAnimation: _previewAnimation,
          )
        ],
      ),
    );
  }

  Widget _buildInterface() {
    return Stack(
      children: <Widget>[
        SafeArea(
          bottom: false,
          child: TopBarWidget(
            captureMode: _captureMode,
            switchFlash: _switchFlash,
            orientation: _orientation,
            rotationController: _iconsAnimationController,
            onFlashTap: () {
              switch (_switchFlash.value) {
                case CameraFlashes.NONE:
                  _switchFlash.value = CameraFlashes.ON;
                  break;
                case CameraFlashes.ON:
                  _switchFlash.value = CameraFlashes.AUTO;
                  break;
                case CameraFlashes.AUTO:
                  _switchFlash.value = CameraFlashes.ALWAYS;
                  break;
                case CameraFlashes.ALWAYS:
                  _switchFlash.value = CameraFlashes.NONE;
                  break;
              }
              setState(() {});
            },
          ),
        ),
        BottomBarWidget(
          leadingChild: OptionButton(
            icon: Icons.close,
            rotationController: _iconsAnimationController,
            orientation: _orientation,
            onTapCallback: () {},
          ),
          trailingChild: OptionButton(
            icon: Icons.switch_camera,
            rotationController: _iconsAnimationController,
            orientation: _orientation,
            onTapCallback: () => _onChangeSensorTap(),
          ),
          onCaptureTap:
              (_captureMode.value == CaptureModes.PHOTO) ? _takePhoto : null,
          rotationController: _iconsAnimationController,
          orientation: _orientation,
          captureMode: _captureMode,
        ),
      ],
    );
  }

  _takePhoto() async {
    if (_isTakingPicture) {
      return;
    }
    _updateTakingPictureStatus(true);
    final Directory extDir = await getTemporaryDirectory();
    final testDir =
        await Directory('${extDir.path}/vnpt_iqms').create(recursive: true);
    final String filePath =
        '${testDir.path}/${DateTime.now().millisecondsSinceEpoch}.jpg';

    await _pictureController.takePicture(filePath);
    // lets just make our phone vibrate
    HapticFeedback.vibrate();
    _lastPhotoPath = filePath;
    setState(() {});
    if (_previewAnimationController.status == AnimationStatus.completed) {
      _previewAnimationController.reset();
    }
    _previewAnimationController.forward();
    // final file = File(filePath);
    print('==> PATH_FILE:: $filePath');
    Future.delayed(Duration(seconds: 1)).then(
      (_) {
        _updateTakingPictureStatus(false);
      },
    );
  }

  _onOrientationChange(CameraOrientations newOrientation) {
    _orientation.value = newOrientation;
  }

  _onPermissionsResult(bool granted) {
    if (!granted) {
      AlertDialog alert = AlertDialog(
        title: Text('Error'),
        content: Text(
            'It seems you doesn\'t authorized some permissions. Please check on your settings and try again.'),
        actions: [
          // FlatButton(
          //   child: Text('OK'),
          //   onPressed: () => Navigator.of(context).pop(),
          // ),
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );
    } else {
      setState(() {});
      print("granted");
    }
  }

  Widget _buildCameraFull() {
    return Positioned(
      top: 0,
      left: 0,
      bottom: 0,
      right: 0,
      child: Center(
        child: CameraAwesome(
          onPermissionsResult: _onPermissionsResult,
          selectDefaultSize: (availableSizes) {
            if (availableSizes.isNotEmpty)
              this._photoSize.value = availableSizes[0];
            setState(() {});
            return availableSizes[0];
          },
          captureMode: _captureMode,
          photoSize: _photoSize,
          sensor: _sensor,
          enableAudio: ValueNotifier(false),
          switchFlashMode: _switchFlash,
          zoom: ValueNotifier(0),
          onOrientationChanged: _onOrientationChange,
          onCameraStarted: () {
            // camera started here -- do your after start stuff
          },
        ),
      ),
    );
  }
}
